# HW/SW Codesign Lab #

## Tasks ##
- Introduce TIE instructions in hot-spots of FFT/IFFT (DIT & DIF)
- Hold constants in TIE states or tables
- Compare DIT/DIF
- In the end, FFT should work with selected N with power of 2
## Parallelization ##
- MAC operation: Butterfly Compute Node
- SIMD: Butterflies of one stage can be computed independently
- FLIX: Load input data simultaneously by using the two available DMEMs of coreLX5_hwswcd
